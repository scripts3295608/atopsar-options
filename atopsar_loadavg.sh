##ATOPSAR LOAD AVG SCRIPT##
#!/bin/bash

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -p -r $i -b 00:00 -e 23:59 | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

echo -e "Peak Server Load Times\nDate       Time     Load"
sort -k7 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$7}'

rm -rf /tmp/atopsar*

##End of ATOPSAR LOAD AVG SCRIPT##
