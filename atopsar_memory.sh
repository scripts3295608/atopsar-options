##currently showing all column headers, possible to get processes using most mem at peak times?
#!/bin/bash

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -m -r /var/log/atop/atop_20240222 | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

header=$(awk 'FNR == 6 {print}' /tmp/atopsar.report | sed s/"dirty"// | sed s/"_mem"// | sed s/"slabmem"// | cut -f1 -d" " --complement | sed s/"buffers"//)

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

#echo -e "Peak Memory Usage\ndate       time    $header"
echo -e "Peak Memory Usage Times\ndate       time     total used  cache swap  swap-free"


sort -k3 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{for(i=1;i<=NF;i++)if(i!=5 && i!=7 && i!=8)printf "%s ",$i;print""}'

rm -rf /tmp/atopsar*
