##ATOPSAR DISK SCRIPT##
#!/bin/bash

ls -l /var/log/atop | awk '{print $9}' | grep -v 'daily' > /tmp/file.txt

sed -i '1d' /tmp/file.txt
sed -i 's#^#/var/log/atop/#' /tmp/file.txt

while read i; do
atopsar -S -l -r $i -b 00:00 -e 23:59 | awk 'BEGIN {DATE_STAMP=""; } /analysis date: /{DATE_STAMP=$4;} /^[0-9]/ {print DATE_STAMP, $0;}' >> /tmp/atopsar.report
done < /tmp/file.txt

grep -v "analysis date:" /tmp/atopsar.report > /tmp/atopsar_numbers.report

echo -e "Peak Write IOPS\nDate       Time     Write/s"
sort -k7 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$7}'
echo -e "Peak Read IOPS\nDate       Time     Read/s"
sort -k5 -rn /tmp/atopsar_numbers.report | head -n5 | awk '{print $1,$2,$5}'

rm -rf /tmp/atopsar*
